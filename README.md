blackjack by Khalim Husain
=========

A console based blackjack game developed in C++.

NOTE: Requires a C++11 compiler

Features

* 1 to 6 human players vs. the dealer (CPU)
* Betting
* Splitting and double down
* Saves player statistics (money, wins, losses, draws) locally

Current TODOs:

* Thorough testing

Goals of this project:

* Develop a blackjack game in C++ independently
* Port it to Android to gain more experience with native Android development
* Create an attractive and easy to use GUI
* Add online multiplayer on Android
* Add online storage option so player statistics aren't tied to a single device