#include <iostream>
#include <string>

#include "card.h"

// Constructor
Card::Card(Rank r, Suit s)
{
	m_rank = r;
	m_suit = s;
}

// Returns the rank of a given card
Rank Card::getRank()
{
	return m_rank;
}

// Returns the suit of a given card
Suit Card::getSuit()
{
	return m_suit;
}

// Returns the value of a given card
unsigned int Card::getValue()
{
	unsigned int cardVal = (unsigned int)getRank();
	
	if (cardVal == 1) 
	{
		return 11;
	} 
	else if (cardVal > 10) 
	{
		return 10;
	} 
	else 
	{
		return cardVal;
	}
}

// Outputs a given card information
ostream& operator<<(ostream& os, Card& c)
{
	string suits[SUIT_COUNT] = 
	{ 
		"Clubs", 
		"Diamonds", 
		"Hearts", 
		"Spades" 
	};
	
	string ranks[RANK_COUNT] = 
	{ 
		"Ace", 
		"Two", 
		"Three", 
		"Four", 
		"Five", 
		"Six",
		"Seven",
		"Eight",
		"Nine",
		"Ten",
		"Jack",
		"Queen",
		"King"
	};

	os << ranks[c.getRank() - 1] << " of " << suits[c.getSuit()];
	return os;
}
