#ifndef CARD_H
#define CARD_H

using namespace std;

enum Rank
{
	Ace = 1,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	RANK_COUNT = 13
};

enum Suit
{
	Clubs,
	Diamonds,
	Hearts,
	Spades,
	SUIT_COUNT
};

class Card {
private:
	Rank m_rank;
	Suit m_suit;
public:
	Card(Rank, Suit);

	Rank getRank();

	Suit getSuit();

	unsigned int getValue();

	friend ostream& operator<<(ostream& os, Card& c);
};

#endif /* CARD_H */
