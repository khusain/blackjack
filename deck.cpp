#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>

#include "card.h"
#include "deck.h"

using namespace std;

// Default constructor
Deck::Deck()
{
	m_cards = createDeck(1);
}

// Constructor for creating numDecks decks
Deck::Deck(unsigned int numDecks)
{
	if (numDecks == 0)
	{
		cerr << "Cannot create an empty deck. Defaulting to 1 deck" << endl;
		numDecks = 1;
	}
	
	m_cards = createDeck(numDecks);
}

// Creates numDecks decks of cards
vector<Card> Deck::createDeck(unsigned int numDecks)
{
	vector<Card> cards;

	for (unsigned int deck = 0; deck < numDecks; deck++)
	{
		for (unsigned int i = 0; i < SUIT_COUNT; i++)
		{
			// Start at 1 because Ace = 1 in the enum
			for (unsigned int j = 1; j < RANK_COUNT + 1; j++)
			{
				cards.push_back(Card(Rank(j),Suit(i)));
			}
		}
	}

	return cards;
}

// Returns the size of a deck
unsigned int Deck::size()
{
	return m_cards.size();
}

// Shuffles the cards in a deck
void Deck::shuffle()
{	
        srand(unsigned(time(0)));
        random_shuffle( m_cards.begin(), m_cards.end() );
}

// Get a single card from the deck
Card Deck::getCard()
{
	Card card = m_cards.back();
	m_cards.pop_back();
	return card;
}
