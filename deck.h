#ifndef DECK_H
#define DECK_H

#include "card.h"

using std::vector;

class Deck {
private:
	vector<Card> m_cards;
	vector<Card> createDeck(unsigned int numDecks);
public:
	Deck();
	
	Deck(unsigned int numDecks);
	
	unsigned int size();

	void shuffle();

	Card getCard();
};

#endif /* DECK_H */
