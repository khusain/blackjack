#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <climits>
#include <sstream>

#include "card.h"
#include "hand.h"
#include "deck.h"
#include "player.h"
#include "game.h"

using namespace std;

// Constructor
Game::Game(vector<Player> players) : m_players(players)
{
}

// Returns the number of players in this game
unsigned int Game::numPlayers()
{
	return m_players.size();
}

// Determine if a player has money to play in the next round
void Game::determinePlayability()
{
	for (unsigned int i = 0; i < numPlayers() - 1; i++)
	{
		if ( m_players.at(i).getMoney() == 0)
		{
			m_players.at(i).setInactive();
		}
	}
}

// Betting for a round
void Game::bets()
{
	for (unsigned int i = 0; i < numPlayers() - 1; i++)
	{
		int bet = 0;
		unsigned int money = m_players.at(i).getMoney();

		while (bet == 0 && m_players.at(i).getActive())
		{
			cout << m_players.at(i).name() << ", you currently have $";
			cout << m_players.at(i).getMoney() << ". Place your bet: $";

			string line;
			getline(std::cin, line);
			stringstream ss(line);

			if (cin.fail() || ! (ss >> bet && ss.eof()))
			{
				cout << "Invalid input. Only integer values allowed" << endl;
				bet = 0;
			}
			else if (bet < 0)
			{
				cout << "Cannot bet a negative amount" << endl;
				bet = 0;
			}
			else if ((unsigned int)bet > money)
			{
				cout << "You do not have that much money" << endl;
				bet = 0;
			}
			else if (bet == 0)
			{
				cout << "Cannot bet nothing" << endl;
			}
		}

		m_players.at(i).addHand(bet);
	}
}		

// Deal two cards to each player that is participating in this round
void Game::deal()
{
	m_cards.shuffle();
	
	for (unsigned int i = 0; i < numPlayers(); i++)
	{
		if (m_players.at(i).getActive())
		{
			cout << endl;
			hit(i,0);
			hit(i,0);
		}
	}
}

// Play a round
bool Game::play()
{
	unsigned int compPlayNum = numPlayers() - 1;
	cout << endl;
	bets();
	cout << endl;
 	deal();

	// If computer got blackjack, end the round	
	if (m_players.at(compPlayNum).getBlackjack(0))
	{
		for (unsigned int i = 0; i < compPlayNum; i++)
		{
			m_players.at(i).setStay(0);
		}
	}
	else
	{
		for (unsigned int i = 0; i < compPlayNum; i++)
		{
			if ( (! m_players.at(i).getBlackjack(0)) && m_players.at(i).getActive() )
			{
				cout << endl << "It is now " << m_players.at(i).name() << "'s turn" << endl;
				playerTurn(i);
			}
		}

		cout << endl;
		computerTurn();
		cout << endl;
	}
	
	determineWinners();
	cout << endl;
	outputStatistics();
	cout << endl;
	return playAgain();
}

// Logic for each human player's turn
void Game::playerTurn(unsigned int playerNum)
{
	unsigned int curHand = 0;

	while (curHand < m_players.at(playerNum).numHands())
	{
		cout << "Playing hand " << curHand + 1 << endl;

		// In case of a new hand due to split, we need to give the player a second card
		if (m_players.at(playerNum).numCards(curHand) == 1)
		{
			hit(playerNum, curHand);
		}

		while (! (m_players.at(playerNum).getStay(curHand) || m_players.at(playerNum).getBust(curHand)) )
		{
			string input;

			cout << outputTotal(playerNum, curHand, true) << endl;
			unsigned int playerBet = m_players.at(playerNum).getBet(curHand);
			bool canDoubleDown = m_players.at(playerNum).getMoney() >= playerBet;
			bool splittable = (m_players.at(playerNum).splittableHand(curHand) && canDoubleDown);

			if (splittable)
			{
				cout << "Hit (h), stay (s), double down (d), or split (p): ";
		 	}
			else if (canDoubleDown)
			{
				cout << "Hit (h), stay (s), or double down (d): ";
			}
			else
			{
				cout << "Hit (h), or stay (s): ";
			}

			std::getline(cin, input);

			if (input == "h" || input == "H")
			{
				hit(playerNum, curHand);
			}
			else if (input == "s" || input == "S")
			{
				m_players.at(playerNum).setStay(curHand);
			}
			else if ( (input == "p" || input == "P") && splittable)
			{
				m_players.at(playerNum).splitHands(curHand);
				hit(playerNum, curHand);
			}
			else if ( (input == "d" || input == "D") && canDoubleDown)
			{
				m_players.at(playerNum).removeMoney(playerBet);
				m_players.at(playerNum).setBet(curHand, playerBet * 2);
				hit(playerNum, curHand);
				m_players.at(playerNum).setStay(curHand);
			}
			else
			{
				cout << "Invalid choice" << endl;
			}
		}
		cout << outputTotal(playerNum, curHand, false) << endl;
		curHand++;
	}
}

// Logic for computer's turn
void Game::computerTurn()
{
	unsigned int playerNum = numPlayers() - 1;
	
	cout << "It is now the computer's turn" << endl;
	m_players.at(playerNum).cardInHand(0, (unsigned int)1);

	while (! ( m_players.at(playerNum).getStay(0) 
			|| ( m_players.at(playerNum).getBust(0) )
			|| (m_players.at(playerNum).total(0) > 16) ) )
	{
		cout << outputTotal(playerNum, 0, true) << endl;
		hit(playerNum, 0);
	}
	
	unsigned int compTotal = m_players.at(playerNum).total(0);
	
	if (compTotal >= 17 && compTotal < 21)
	{
		cout << "STAY at " << compTotal << endl;
	}
}

// Logic for when player chooses to hit
void Game::hit(unsigned int playerNum, unsigned int handNum)
{
	if (m_players.at(playerNum).numHands() < handNum + 1)
	{
		m_players.at(playerNum).addHand();
	}	

	if (m_cards.size() > 0)
	{
		unsigned int numCards = m_players.at(playerNum).numCards(handNum);

		m_players.at(playerNum).addCard(handNum, m_cards.getCard());

		// Computer's second card is face down until human players have played
		if (! (playerNum == numPlayers() - 1 && numCards == 1) )
		{
			m_players.at(playerNum).cardInHand(handNum,numCards);
		}

		determineNewStatus(playerNum, handNum);
	}
	else
	{
		cerr << "ERROR: No more cards left" << endl;
	}
}

// Determine player's specific hand status
void Game::determineNewStatus(unsigned int playerNum, unsigned int handNum)
{
	unsigned int total = m_players.at(playerNum).total(handNum);
	unsigned int numCards = m_players.at(playerNum).numCards(handNum);

	if (total > 21)
	{
		m_players.at(playerNum).setBust(handNum);
		cout << "BUST: " << total << endl;
		return;
	}
	else if (total == 21)
	{
		m_players.at(playerNum).setStay(handNum);

		if (numCards == 2)
		{
			m_players.at(playerNum).setBlackjack(handNum);

			if (playerNum == numPlayers() - 1)
			{
				cout << "Computer has ";
			}
			
			cout << "BLACKJACK!" << endl;
			return;
		}
		else
		{
			cout << "STAY: 21" << endl;
			return;
		 }
	}
	
	// Automatically stay if player has 5 cards	
	if (numCards == 5 && 
		! (m_players.at(playerNum).getBust(handNum) 
		|| m_players.at(playerNum).getStay(handNum)))
	{
		m_players.at(playerNum).setStay(handNum);
	}
}

// Outputs the player's hand total
string Game::outputTotal(
	unsigned int playerNum, 
	unsigned int handNum, 
	bool current
) {
	string total;

	if (current)
	{
		total = "current ";
	}

	total += "total on hand " + to_string(handNum + 1) 
			+ " is: " + to_string(m_players.at(playerNum).total(handNum));

	if (m_players.at(playerNum).getSoftAce(handNum))
	{
		total += " (soft)";
	}
	
	if (m_players.at(playerNum).splittableHand(handNum))
	{
		if (m_players.at(playerNum).getMoney() < m_players.at(playerNum).getBet(handNum))
		{
			total += " [Cannot split: not enough money]";
		} 
		else
		{
			total += " (splittable)";
        }
	}

	return total;
}

// Determines the winner for the round
void Game::determineWinners()
{
	unsigned int compPlayerNum = numPlayers() - 1;
	unsigned int compTotal = m_players.at(compPlayerNum).total(0);
	bool compBust = m_players.at(compPlayerNum).getBust(0);
	bool compBlackjack = m_players.at(compPlayerNum).getBlackjack(0);	

	for (unsigned int i = 0; i < compPlayerNum; i++)
	{
		unsigned int curHand = 0;
		
		while (curHand < m_players.at(i).numHands() && m_players.at(i).getActive())
		{
			unsigned int handTotal = m_players.at(i).total(curHand);
			unsigned int bet = m_players.at(i).getBet(curHand);
			cout << m_players.at(i).name();

			/* 
			 * Player blackjack prioritizes everything because players are dealt cards first
		  	 * Computer blackjack follows
		  	 * After that, player victory depends on if they bust and their total vs. computers
			*/

			if (m_players.at(i).getBlackjack(curHand))
			{
				cout << " wins $" << 2 * bet;
				m_players.at(i).addWin();
				m_players.at(i).addMoney(2 * bet);
			}
			else if (m_players.at(i).getBust(curHand) || compBlackjack)
			{
				cout << " loses $" << bet;
				m_players.at(i).addLoss();
			}
			else if (compBust || handTotal > compTotal)
			{
				cout << " wins $" << 2 * bet;
				m_players.at(i).addWin();
				m_players.at(i).addMoney(2 * bet);
			}
			else if (handTotal == compTotal)
			{
				cout << " pushes (gets back the $" << bet << " bet)";
				m_players.at(i).addDraw();
				m_players.at(i).addMoney(bet);
			}
			else
			{
				cout << " loses $" << bet;
				m_players.at(i).addLoss();
			}

			string handName[] = 
			{
				"first",
				"second",
				"third",
				"fourth",
				"fifth",
				"sixth",
				"seventh",
				"eighth",
				"ninth",
				"tenth"
			};
	
			cout << " on their " << handName[curHand] << " hand" << endl;
			curHand++;
		}
	}
}

// Output statistics to stdout for the game so far
void Game::outputStatistics()
{
	unsigned int nameWidth = 0, moneyWidth = 0, otherWidth = 0, numHeadings = 3;

	// Determine ideal widths for statistics
	for (unsigned int i = 0; i < numPlayers() - 1; i++)
	{
		ostringstream convert;
		unsigned int result;

		// Check if player name is the longest found so far
		if (m_players.at(i).name().length() > nameWidth)
		{
			nameWidth = m_players.at(i).name().length();
		}

		convert << m_players.at(i).getMoney();
		result = convert.str().length();

		// Check if player money digits is the longest found so far
		if (result > moneyWidth)
		{
			moneyWidth = result;
		}

		// Store player wins, losses and draws in array to make a for loop possible
		unsigned int record[] =
		{
			m_players.at(i).getWins(),
			m_players.at(i).getLosses(),
			m_players.at(i).getDraws()
		};

		// Determine if player wins, losses and draws digits are the longest found so far
		for (unsigned int j = 0; j < numHeadings; j++)
		{
			convert.str(std::string());
			convert << record[j];
			result = convert.str().length();

			if (result > otherWidth)
			{
				otherWidth = result;
			}
		}
	}

	nameWidth += 1;
	moneyWidth += 2;
	otherWidth += 2;

	cout << "Statistics for human players" << endl;

	for (unsigned int i = 0; i < numPlayers() - 1; i++)
	{
		string headings[] =
		{
			"Wins: ",
			"Losses: ",
			"Draws: "
		};

		unsigned int winLossDraws[] =
		{
			m_players.at(i).getWins(),
			m_players.at(i).getLosses(),
			m_players.at(i).getDraws()
		};

		// Output name
		cout << left << setw(nameWidth) << setfill(' ');
		cout << m_players.at(i).name() << " - ";
		
		// Output money
		cout << "Money: $";
		cout << setw(moneyWidth) << setfill(' '); 
		cout << m_players.at(i).getMoney();
		
		// Output wins, losses and draws
		for (unsigned int j = 0; j < numHeadings; j++)
		{
			cout << headings[j] << setw(otherWidth) << setfill(' ') << winLossDraws[j];
		}

		cout << endl;
	}
}


// Output statistics to a vector
// Each player stats in a string in the form 'name, money, wins, losses, draws'
vector<string> Game::outputStatisticsCSV()
{
	vector<string> stats;

	for (unsigned int i = 0; i < numPlayers() - 1; i++)
	{
		stats.push_back(m_players.at(i).name() + ","
			+ to_string(m_players.at(i).getMoney()) + ","
			+ to_string(m_players.at(i).getWins()) + ","
			+ to_string(m_players.at(i).getLosses()) + ","
			+ to_string(m_players.at(i).getDraws()));
	}

	return stats;
}

// Determines if the user can/wants to play again
bool Game::playAgain()
{
	while (true)
	{
		string again;
		bool canPlay = false;

		determinePlayability();

		for (unsigned int i = 0; i < numPlayers() - 1; i++)
		{
			if (m_players.at(i).getActive())
			{
				canPlay = true;
			}
		}

		if (canPlay)
		{
			cout << "Play again? (y/n): ";
			std::getline(cin, again);
	
			if (again == "y" || again == "Y")
			{
				return true;
			}
			else if (again == "n" || again == "N")
			{
				return false;
			}
			else
			{
				cout << "Invalid response" << endl;
			}
		}
		else
		{
			cout << "No players have any money left. Ending game." << endl;
			return false;
		}
	}
}

// Resets the game
void Game::reset()
{
	for (unsigned int i = 0; i < numPlayers(); i++)
	{
		m_players.at(i).reset();
	}

	Deck deck;
	m_cards = deck;
}
