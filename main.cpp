#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <string>
#include <vector>
#include <climits>
#include <fstream>
#include <algorithm>

#include "card.h"
#include "game.h"
#include "player.h"

using namespace std;

// Amount of money that a new player starts with
const unsigned int startingMoney = 100;

// File that user statistics is stored in
const char* userStatsFileName = "userStats.txt";

// Temporary file for updating user statistics
const char* tempStatsFileName = "tempStats.txt";

// Maps a saved player to the line number in the statistics file
vector<pair<unsigned int,unsigned int>> playerLocationMap;

// Parses the user statistics file and pushes valid entries into a Player vector
vector<Player> getStoredPlayers()
{
	vector<Player> players;
	ifstream myfile (userStatsFileName);

	if (myfile.is_open())
	{
		string line;
		unsigned int lineCount = 0;

		while (getline(myfile,line))
		{
			string name;
			unsigned int money;
			unsigned int wins;
			unsigned int losses;

			lineCount++;

			// Check if the user statistic format is correct
			if (! (count(line.begin(), line.end(), ',') == 4) )
			{
				cout << "Improper format of user statistic at line "
					 << lineCount << " of " << userStatsFileName << ": " << line << endl;
				cout << "Proper format: name,money,wins,losses,draws" << endl;
				return players;
			}

			unsigned int stat = 0;
			string temp = "";
			// Go through the line and extract name, money, wins, losses and draws
			for (unsigned int i = 0; i < line.size(); i++)
			{
				if (line[i] == ',')
				{
					if (stat == 0)
					{
						name = temp;
					}
					else if (stat == 1)
					{
						money = stoi(temp);
					}
					else if (stat == 2)
					{
						wins = stoi(temp);
					}
					else if (stat == 3)
					{
						losses = stoi(temp);
					}
					else
					{
						cout << "ERROR" << endl;
						return players;
					}

					stat++;
					temp = "";
				}
				else
				{
					temp += line[i];
				}
			} //endfor

			// Add properly formatted user into the players vector
			if (stat == 4)
			{
				players.push_back(Player(name, money, wins, losses, stoi(temp), true));
			}
			else
			{
				cerr << "ERROR: Invalid statistic on line " << lineCount << ": " << line << endl;
			}
		} // endwhile
		myfile.close();
	}
	else
	{
		cout << "No user statistics file found" << endl;
	}

	return players;
}

// Constructs a vector of players for this game
vector<Player> determinePlayers()
{
	vector<Player> storedPlayers = getStoredPlayers();
	vector<Player> players;
	unsigned int numHumanPlayers;
	
	cout << "How many human players will be playing today? ";

	cin >>  numHumanPlayers;
	
	if (cin.fail() || numHumanPlayers == 0)
	{
		cout << "Invalid input. Defaulting to 1 human player" << endl;
		numHumanPlayers = 1;
	}
	else if (numHumanPlayers > 6)
	{
		cout << "This game only supports a max of 6 human players. Defaulting to 6 human players" << endl;
		numHumanPlayers = 6;
	}

	cin.clear();
	cin.ignore(INT_MAX, '\n');
	cout << endl;
	
	for (unsigned int i = 0; i < numHumanPlayers; i++)
	{
		string playerName;
		bool found = false;

		cout << "What is player " << i + 1 << "'s name? ";
		std::getline(cin, playerName);

		// Check if user already exists in the statistics file and if so, use that data 
		for (unsigned int j = 0; j < storedPlayers.size(); j++)
		{
			if (storedPlayers[j].name() == playerName)
			{
				cout << "Using saved data for " << playerName << endl;
				playerLocationMap.push_back(pair<int,int>(i,j+1));

				// If player had no money at the end of the last time they played, give them the same amount as new players get
				if (storedPlayers[j].getMoney() == 0)
				{
					cout << "The last time you played you ended with no money. Here's $" << startingMoney << endl;
					storedPlayers[j].addMoney(startingMoney);
				}

				players.push_back(storedPlayers[j]);
				found = true;
			}
		}

		if (! found)
		{
			players.push_back(Player(playerName, startingMoney, true));
		}
	}

	// Add computer player
	players.push_back(Player("Computer", startingMoney, false));

	return players;
}


// Add user statistic data to the statistics file
void saveUserStats(vector<string> stats)
{
	// Iterate through all players of this game
	for (unsigned int i = 0; i < stats.size(); i++)
	{		
		bool newFile = false;

		// Check if  player i has locally saved data	
		for (auto it = playerLocationMap.begin(); it != playerLocationMap.end(); it++)
		{
		   unsigned int vecPlayerNum = it->first;
		   unsigned int vectLineNum = it->second;

			// Check if player i is found in playerLocationMap
			if (vecPlayerNum == i)
			{
				newFile = true;
				ifstream myfile (userStatsFileName);
				ofstream myfile2 (tempStatsFileName);

				// Create temp file that is identical to locally saved data but with new statistics for player i 
				if (myfile.is_open() && myfile2.is_open())
				{
					string line;
					unsigned int lineCount = 0;
					
					while (getline(myfile,line))
					{
						lineCount++;
				
						// Determine if the current line of stats file contains player i info
						if (lineCount == vectLineNum)
						{
							myfile2 << stats[i] << endl;
						}
						else
						{
							myfile2 << line << endl;
						}
					}
				}

				myfile.close();
				myfile2.close();
				break;	
			}
		}

		// If temp file was created, copy data to user stats file and delete temp file 
		if (newFile)
		{
			ifstream src(tempStatsFileName, std::ios::binary);
			ofstream dst(userStatsFileName, std::ios::binary);
			dst << src.rdbuf();

			src.close();
			dst.close();
			remove(tempStatsFileName);
		}
		else
		{
			// adds user information to end of user statistics file
			ofstream myfile;
		  	myfile.open (userStatsFileName, ios::app);
			myfile << stats[i] << endl;
			myfile.close();
		}
	}
}

int main()
{
	cout << "Welcome to blackjack by Khalim Husain" << endl;
	
	Game game = Game(determinePlayers());

	while (true) {
		game.reset();
		
		if (! game.play())
		{
			break;
		}
	}

	// Grabs user statistics of this game. Each user is a CSV string
	vector<string> stats = game.outputStatisticsCSV();
	
	saveUserStats(stats);	
	
	cout << "Thank you for playing" << endl;

	return 0;
}
