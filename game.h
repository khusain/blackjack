#ifndef GAME_H
#define GAME_H

#include "card.h"
#include "deck.h"
#include "player.h"

using std::vector;

class Game {
private:
	Deck m_cards;
	vector<Player> m_players;
public:
	Game(vector<Player> players);

	unsigned int numPlayers();

	// Begin round
	void determinePlayability();

	void bets();

	void deal();

	bool play();

	// Per player
	void playerTurn(unsigned int playerNum);

	void computerTurn();

	void hit(unsigned int playerNum, unsigned int handNum);

	void determineNewStatus(unsigned int playerNum, unsigned int handNum);

	string outputTotal(
		unsigned int playerNum, 
		unsigned int handNum, 
		bool current
	);

	// End round/game
	void determineWinners();

	void outputStatistics();

	vector<string> outputStatisticsCSV();

	bool playAgain();

	void reset();
};

#endif /* GAME_H */
