#include <iostream>
#include <string>
#include <vector>

#include "card.h"
#include "hand.h"

using namespace std;

// Default constructor
Hand::Hand()
{
	m_bet = 0;
	m_softAce = false;
	m_blackjack = false;
	m_stay = false;
	m_bust = false;
}

// Constrctor for hand with a given bet
Hand::Hand(unsigned int bet)
{
	m_bet = bet;
	m_softAce = false;
	m_blackjack = false;
	m_stay = false;
	m_bust = false;
}

// Returns the number of cards in the hand
unsigned int Hand::numCards()
{
	return m_hand.size();
}

// Outputs a specific card
void Hand::cardInHand(unsigned int cardNum)
{
	cout << m_hand.at(cardNum); 
}

// Output the hand total
unsigned int Hand::total()
{
	unsigned int total = 0;
	unsigned int numAces = 0;

	for (unsigned int i = 0; i < m_hand.size(); i++)
	{
		unsigned int cardVal = m_hand.at(i).getValue();

		if (cardVal == 11)
		{

			if (! (m_softAce && numAces == 0))
			{
				cardVal = 1;
			}

			numAces++;
	
		}

		total += cardVal;
	}

	if (total > 21 && m_softAce)
	{
		total -= 10;
		m_softAce = false;
	}
	
	return total;
}

// Returns the bet on the hand
unsigned int Hand::getBet()
{
	return m_bet;
}

// Sets the bet on the hand
void Hand::setBet(unsigned int bet)
{
	m_bet = bet;
}

// Remove the last card in a hand
// Used when splitting a hand
Card Hand::removeLastCard()
{
	Card last = m_hand.back();
	m_hand.pop_back();

	return last;
}

// Add a specific card to the hand
void Hand::addCard(Card c)
{
	if (c.getValue() == 11)
	{
		m_softAce = true;
	}

	m_hand.push_back(c);
}

// Determine if a hand is splittable
bool Hand::splittableHand()
{
	if ( (numCards() == 2)&& (m_hand.at(0).getRank() == m_hand.at(1).getRank()) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Returns TRUE if Ace is soft in the hand. FALSE otherwise
bool Hand::getSoftAce()
{
	return m_softAce;
}

// Sets the status of the soft Ace
void Hand::setSoftAce()
{
	m_softAce = true;
}

// Returns TRUE is the hand status is blackjack. FALSE otherwise
bool Hand::getBlackjack()
{
	return m_blackjack;
}

// Set the hand status to "blackjack"
void Hand::setBlackjack()
{
	m_blackjack = true;
}

// Returns TRUE if the hand status is "stay". FALSE otherwise
bool Hand::getStay()
{
	return m_stay;
}

// Set the hand status to "stay"
void Hand::setStay()
{
	m_stay = true;
}

// Returns TRUE is the hand status is "bust". FALSE otherwise
bool Hand::getBust()
{
	return m_bust;
}

// Set the hand status to "bust"
void Hand::setBust()
{
	m_bust = true;
}
