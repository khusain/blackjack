CC=g++
CFLAGS=--std=c++11 -c -Wall
LDFLAGS=
SOURCES=main.cpp card.cpp deck.cpp hand.cpp player.cpp game.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=blackjack

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) $(EXECUTABLE)
	$(RM) $(OBJECTS)

run:
	@$(MAKE) && ./$(EXECUTABLE)
