#include <iostream>
#include <string>
#include <vector>

#include "card.h"
#include "hand.h"
#include "player.h"

using namespace std;

// Constructor for new player
Player::Player(
	string name,
	unsigned int money,
	bool isHuman
) : m_name(name) {
	m_money = money;
	m_isHuman = isHuman;
	m_active = true;
	m_wins = 0;
	m_losses = 0;
	m_draws = 0;
}

// Constructor for existing player
Player::Player(
	string name,
	unsigned int money,
	unsigned int wins,
	unsigned int losses,
	unsigned int draws,
	bool isHuman
) : m_name(name) {
	m_money = money;
	m_isHuman = isHuman;
	m_active = true;
	m_wins = wins;
	m_losses = losses;
	m_draws = draws;
}

// Returns the player's name
string Player::name()
{
	return m_name;
}

// Returns TRUE if player is a human. FALSE otherwise
bool Player::isHuman()
{
	return m_isHuman;
}

// Returns TRUE is player is active. FALSE otherwise
bool Player::getActive()
{
	return m_active;
}

// Sets the player to active
void Player::setActive()
{
	m_active = true;
}

// Sets the player to inactive
void Player::setInactive()
{
	m_active = false;
}

// Returns the number of hands that the player has this round
unsigned int Player::numHands()
{
	return m_hand.size();
}

// Add a hand for the player
void Player::addHand()
{
	Hand a;
	m_hand.push_back(a);
}

// Add a hand for the player with a given bet amount attached to it
void Player::addHand(unsigned int bet)
{
	Hand a(bet);
	m_hand.push_back(a);
	m_money -= bet;
}

// Resets the player's hand
void Player::reset()
{
	m_hand.clear();
}

// Gets the amount of money that the player has
unsigned int Player::getMoney()
{
	return m_money;
}

// Increment the amount of money by a given amount
void Player::addMoney(unsigned int amount)
{
	m_money += amount;
}

// Decrement the amount of money by a given amount
void Player::removeMoney(unsigned int amount)
{
	m_money -= amount;
}

// Returns the amount of cards in a specific hand
unsigned int Player::numCards(unsigned int handNum)
{
	return m_hand.at(handNum).numCards();
}

// Outputs what cards are in a specific hand
void Player::outputHand(unsigned int handNum)
{
	for (unsigned int i = 0; i < m_hand.at(handNum).numCards(); i++)
	{
		cardInHand(handNum,i);
	}
}

// Gets the bet amount for a specific hand
unsigned int Player::getBet(unsigned int hand)
{
	return m_hand.at(hand).getBet();
}

// Sets the bet amount for a specific hand
void Player::setBet(unsigned int hand, unsigned int bet)
{
	return m_hand.at(hand).setBet(bet);
}

// Splits a hand into two hands. Only called when player has 2 cards in the given handNum
void Player::splitHands(unsigned int handNum)
{
	Card split = m_hand.at(handNum).removeLastCard();
	unsigned int bet = m_hand.at(handNum).getBet();

	addHand(bet);
	addCard(numHands()-1,split);
}


// Outputs a specific card in a given hand
void Player::cardInHand(unsigned int handNum, unsigned int cardNum)
{
	cout << m_name << "'s ";

	string cardNumName[] =
	{
		"first",
		"second",
		"third",
		"fourth",
		"fifth"
	};

	if (cardNum > 5)
	{
		cerr << "ERROR: Player cannot have more than 5 cards in a hand" << endl;
	}

	cout << cardNumName[cardNum] << " card is ";
	m_hand.at(handNum).cardInHand(cardNum);
	cout << endl;
}

// Returns the total of a current hand
unsigned int Player::total(unsigned int handNum)
{
	return m_hand.at(handNum).total();
}

// Add a given card to a given hand
void Player::addCard(unsigned int handNum, Card c)
{
	if (c.getValue() == 11)
	{
		m_hand.at(handNum).setSoftAce();
	}
	
	m_hand.at(handNum).addCard(c);
}

// Determine if a given hand is splittable
bool Player::splittableHand(unsigned int handNum)
{
	return m_hand.at(handNum).splittableHand(); 
}

// Determine if a gien hand has a soft Ace
bool Player::getSoftAce(unsigned int handNum)
{
	return m_hand.at(handNum).getSoftAce();
}

// Given hand has a soft Ace
void Player::setSoftAce(unsigned int handNum)
{
	m_hand.at(handNum).setSoftAce();
}

// Determine if a given hand is a blackjack
bool Player::getBlackjack(unsigned int handNum)
{
	return m_hand.at(handNum).getBlackjack();
}

// Sets hand as a blackjack hand
void Player::setBlackjack(unsigned int handNum)
{
	m_hand.at(handNum).setBlackjack();
}

// Determine if the player "stay"s on a given hand
bool Player::getStay(unsigned int handNum)
{
	return m_hand.at(handNum).getStay();
}

// Set a given hand status to "stay"
void Player::setStay(unsigned int handNum)
{
	m_hand.at(handNum).setStay();
}

// Determine if player has >21 on a given hand
bool Player::getBust(unsigned int handNum)
{
	return m_hand.at(handNum).getBust();
}

// Set a given hand status to "bust"
void Player::setBust(unsigned int handNum)
{
	m_hand.at(handNum).setBust();
}

// Returns the number of time the player has won a game
unsigned int Player::getWins()
{
	return m_wins;
}

// Returns the number of time the player has lost a game
unsigned int Player::getLosses()
{
	return m_losses;
}

// Returns the number of time the player has draw a game
unsigned int Player::getDraws()
{
	return m_draws;
}

// Increment player win count by 1
void Player::addWin()
{
	m_wins += 1;
}

// Increment player loss count by 1
void Player::addLoss()
{
	m_losses += 1;
}

// Increment player draw count by 1
void Player::addDraw()
{
	m_draws += 1;
}
