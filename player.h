#ifndef PLAYER_H
#define PLAYER_H

#include <vector>
#include "card.h"
#include "hand.h"

using std::string;
using std::vector;

class Player {
private:
	string m_name;
	unsigned int m_money;
	bool m_isHuman;
	bool m_active;
	vector<Hand> m_hand;
	unsigned int m_wins;
	unsigned int m_losses;
	unsigned int m_draws;
public:
	// Main player
	Player(
		string name, 
		unsigned int money, 
		bool isHuman
	);

	Player(
		string name,
		unsigned int money,
		unsigned int wins,
		unsigned int losses,
		unsigned int draws,
		bool isHuman
	);

	string name();

	bool isHuman();

	bool getActive();
	
	void setActive();

	void setInactive();
	
	unsigned int numHands();

	void addHand();

	void addHand(unsigned int bet);

	void reset();

	// Money
	unsigned int getMoney();

	void addMoney(unsigned int amount);

	void removeMoney(unsigned int amount);

	// Per hand - main
	unsigned int numCards(unsigned int handNum);

	void outputHand(unsigned int handNum);

	unsigned int getBet(unsigned int hand);

	void setBet(unsigned int hand, unsigned int bet);
	
	void splitHands(unsigned int handNum);
	
	void cardInHand(unsigned int handNum, unsigned int cardNum);	

	unsigned int total(unsigned int handNum);

	void addCard(unsigned int handNum, Card c);

	// Per hand - status
	bool splittableHand(unsigned int handNum);

	bool getSoftAce(unsigned int handNum);

	void setSoftAce(unsigned int handNum);

	bool getBlackjack(unsigned int handNum);

	void setBlackjack(unsigned int handNum);
	
	bool getStay(unsigned int handNum);
	
	void setStay(unsigned int handNum);
	
	bool getBust(unsigned int handNum);
	
	void setBust(unsigned int handNum);

	// Statistics
	unsigned int getWins();
	
	void addWin();

	unsigned int getLosses();
	
	void addLoss();
	
	unsigned int getDraws();

	void addDraw();
};

#endif /* PLAYER_H */
