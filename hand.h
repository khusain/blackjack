#ifndef HAND_H
#define HAND_H

#include <iostream>
#include <string>
#include <vector>

#include "card.h"

using std::vector;

class Hand {
private:
	vector<Card> m_hand;
	unsigned int m_bet;
	bool m_softAce;
	bool m_blackjack;
	bool m_stay;
	bool m_bust;
public:
	Hand();

	Hand(unsigned int bet);
	
	unsigned int numCards();
		
	void cardInHand(unsigned int cardNum);

	unsigned int total();

	unsigned int getBet();

	void setBet(unsigned int bet);

	Card removeLastCard();

	void addCard(Card c);

	// Status
	
	bool splittableHand();

	bool getSoftAce();
	
	void setSoftAce();
	
	bool getBlackjack();

	void setBlackjack();

	bool getStay();

	void setStay();

	bool getBust();
	
	void setBust();
};

#endif /* HAND_H */
